https://blender.stackexchange.com/questions/7378/diff-and-version-manage-blender-work

​           

For best practices I would recommend to learn from larger projects done with blender.  You could look how [Big Bug Bunny](http://graphicall.org/bbb/index.php) was organized.

- First don't have a team editing one large .blend file, have different folders for textures, material libraries, scenes, characters and so on. Each of these file can be versioned in a Version Control System (VCS). 
- Have different scenes which only reference all the used models (link instead of append). This makes it versionable. When someone decides that a model of a character  looked better a week earlier, you could restore this character from the VCS. 
- To track the changes made on an individual item the VCS comment / history should be used.
- Unlike in software development you wouldn't be able to create branches and merge them back into a head revision. Technically this is because the files are binary and no meaningful difference can be extracted for merging. You usually need a branch when your software is deployed to a production environment and develop new features in a branch and fix bugs in the production version or vice versa. When the new features are scheduled for release you would merge features and bug fixes. But this doesn't happen when creating an animation, when a movie is out it can't be bug fixed nor would new features be added. Having that said it is probably not as painful as in the field software development not to have the ability to merge.
- In order to debug settings that were unintentionally changed and lead to undesired effects. The settings could be compared between two revisions of a file as described here: [Get a diff between two .blend files](https://blender.stackexchange.com/questions/7484/get-a-diff-between-two-blend-files)

Which VCS should be used?

This basically depends on the support of your platform used, the experience the team members for details see: